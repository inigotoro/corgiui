import { bindMe } from './utils';

export default class Droppable {
    constructor(selector, options = {}) {
        this.droppableHolders = document.querySelectorAll(selector);
        this.options = Object.assign({
            dragOver: this.dragOver,
            dragEnter: this.dragEnter,
            dragLeave: this.dragLeave,
            drop: this.drop,
        }, options);
        this.addListeners();      
    }

    addListeners() {        
        this.droppableHolders.forEach(holder => {
            bindMe(holder, 'dragover', this.options.dragOver);
            bindMe(holder, 'dragenter', this.options.dragEnter);
            bindMe(holder, 'dragleave', this.options.dragLeave);
            bindMe(holder, 'drop', this.options.drop);
        });
    }

    dragOver(e) {
        e.preventDefault();
        // console.log('dragOver', e);
    }

    dragEnter(e) {
        e.preventDefault();
        // console.log('dragEnter', e);
    }

    dragLeave(e) {
        // console.log('dragLeave', e);
    }

    drop(e) {
        // console.log('drop', e);
    }
}