import components from '../components.json';
import { QuillDeltaToHtmlConverter } from 'quill-delta-to-html'; 

export default class EditPopup {
    constructor(component, bindingFn) {
        if (!component || !Object.keys(components).includes(component.tagName.toLowerCase())) {
            alert('Invalid component');
            return;
        }
        this.component = component;
        this.bindDraggables = bindingFn;
        this.openDialog();
    }

    createOverlay() {
        const { freeText, properties }  = components[this.component.tagName.toLowerCase()];
        const hasOptions = Object.keys(properties).length > 0;
        document.body.insertAdjacentHTML('afterBegin', `
        <div id="mask">
            <form id="popup" ${!freeText ? 'class="popup--scrolled"' : ''}>
                <div id="popup__controls">
                    <div class="popup__scroll popup__scroll--left ${!freeText ? 'hidden' : ''}"></div>
                    <div class="popup__scroll popup__scroll--right ${!freeText || !hasOptions ? 'hidden' : ''}"></div>
                    <div class="popup__close"></div>
                </div>                
                <div id="popup__main">
                    <div id="popup__rte">
                        <div id="popup__rte--input"></div>
                    </div>
                    <div id="popup__fields">
                        
                    </div>
                </div>
                <div id="popup__buttons">
                    <button class="popup__button popup__button--save">Save</button>
                    <button class="popup__button popup__button--cancel">Cancel</button>
                </div>
            </form>
        </div>`);
    }

    addRTE() {
        this.quill = new Quill('#popup__rte--input', {
            modules: {
                toolbar: [
                    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                    ['blockquote', 'code-block'],

                    [{ header: [1, 2, 3, 4, false] }],               // custom button values
                    [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
                    [{ 'direction': 'rtl' }],                         // text direction

                    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                    [{ 'font': [] }],
                    [{ 'align': [] }],

                    ['clean']                                         // remove formatting button
                ]
            },
            placeholder: 'Add your text here...',
            theme: 'snow'
        });

        this.quill.clipboard.dangerouslyPasteHTML(0, this.component.innerHTML);
    }

    saveForm(e) {
        const theForm = e.target;
        theForm.querySelectorAll('.formField').forEach(field => {
            if (!field.value) {
                this.component.removeAttribute(field.name);
            } else {
                this.component.setAttribute(field.name, field.value);
            }
        });
            
        const converter = new QuillDeltaToHtmlConverter(this.quill.getContents().ops, {});        
        const html = converter.convert(); 
        this.component.innerHTML = html;
        this.component.dataset.binded = false;
        this.bindDraggables();
    }

    addBindings() {
        document.querySelectorAll('.popup__close, .popup__button--cancel').forEach(button => {
            button.addEventListener('click', (e) => {
                if (e.preventDefault){
                    e.preventDefault();
                }
                e.target.closest('#mask').remove();
            });
        });
        document.querySelector('.popup__scroll--right').addEventListener('click', (e) => {
            e.target.closest('#popup').classList.add('popup--scrolled');
        });
        document.querySelector('.popup__scroll--left').addEventListener('click', (e) => {
            e.target.closest('#popup').classList.remove('popup--scrolled');
        });
        document.querySelector('#popup').addEventListener('submit', e => {
            e.preventDefault();
            this.saveForm(e);
            e.target.closest('#mask').remove();
        });
    }

    addProperties() {
        const { properties } = components[this.component.tagName.toLowerCase()];
        const fieldHolder = document.querySelector('#popup__fields');
        const initialId = Date.now();
        Object.keys(properties).forEach((elemProp, index) => {
            const id = `${initialId}-${index}`;
            const { optional, type } = properties[elemProp];
            let markup = `<div><label for="${id}">${elemProp} ${!optional ? '<abbr title="required">*</abbr>': ''}</label>`;
            switch(type) {
                case "text": {
                    const oldValue = this.component.getAttribute(elemProp) || '';
                    markup += `<input class="formField" type="text" name="${elemProp}" id="${id}" value="${oldValue}" ${!optional ? 'required' : ''} />`
                }
                break;
            }
            markup += '</div>';
            fieldHolder.insertAdjacentHTML('beforeEnd', markup);
        });
    }

    openDialog() {
        this.createOverlay();
        this.addRTE();
        this.addBindings();
        this.addProperties();
    }
}
