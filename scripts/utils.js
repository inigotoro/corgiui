export function bindMe(elem, event, fn) {
    elem.removeEventListener(event, fn);
    elem.addEventListener(event, fn);
}