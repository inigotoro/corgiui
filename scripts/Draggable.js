import { bindMe } from './utils';

export default class Draggable {
    constructor(selector, options = {}) {
        this.draggableElements = document.querySelectorAll(selector);
        this.options = Object.assign({
            dragStart: this.dragStart,
            dragEnd: this.dragEnd,
        }, options);
        this.addListeners();      
    }    

    addListeners() {        
        this.draggableElements.forEach(element => {
            bindMe(element, 'dragstart', this.options.dragStart);
            bindMe(element, 'dragend', this.options.dragEnd);
        });
    }

    dragStart(e) {
        console.log('dragStart', e);
    }

    dragEnd(e) {
        console.log('dragEnd', e);
    }
}