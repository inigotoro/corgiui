import { bindMe } from './utils';

export default class Resizable {
    constructor(selector, options = {}) {
        this.resizableItems = document.querySelectorAll(selector);
        this.options = Object.assign({
            onLoad: this.onLoad,
            onInitResize: this.defaultInitResize,
            onResize: this.defaultResize,
            onStopResize: this.onStopResize,
        }, options);

        this.executeLogic();
    }

    executeLogic() {
        this.preStopResize = this.preStopResize.bind(this);
        this.preInitResize = this.preInitResize.bind(this);
        this.resizableItems.forEach(item => {
            this.options.onLoad.call(this, item);
            item.querySelectorAll('.resizer').forEach(resizer => {
                bindMe(resizer, 'mousedown', this.preInitResize);
            });
        });
    }

    onLoad(item) {
        console.log('executing load logic for:', item);
    }

    preInitResize(e) {
        const movingElement = e.srcElement.parentElement;
        this.resizingElement = movingElement;
        this.bindedResize = this.options.onResize.bind(this, movingElement);
        window.addEventListener('mousemove', this.bindedResize);      
        window.addEventListener('mouseup', this.preStopResize);
        this.options.onInitResize.call(this, e);
    }

    defaultInitResize(e) {  
        // console.log('resize init', e);        
    }

    defaultResize(e) {
        // console.log('resizing', e);
    }

    preStopResize(e) {
        this.resizingElement.style = '';
        window.removeEventListener('mousemove', this.bindedResize);      
        window.removeEventListener('mouseup', this.preStopResize);
        this.options.onStopResize.call(this, e);
    }

    onStopResize(e) {
        console.log('resize end');
    }
}
