import Draggable from './Draggable';
import Droppable from './Droppable';
import Resizable from './Resizable';
import EditPopup from './EditPopup';

export default class Layout {
    constructor() {
        new Draggable('.drag--draggable', {
            dragStart: this.dragStart.bind(this),
            dragEnd: this.dragEnd.bind(this),
        });
        new Droppable('.drag--droppable', {
            dragEnter: this.dragEnter,
            dragLeave: this.dragLeave,
            drop: this.drop.bind(this),
        });
        this.bindResizables();
    }

    bindResizables() {
        new Resizable('.grid > *:not([data-binded="true"])', {
            onLoad: this.resizableLoad.bind(this),
            onResize: this.resizableResize.bind(this),
            onStopResize: this.resizableStop.bind(this),
            onInitResize: this.resizableInit.bind(this),
        });
    }

    createSpanElement(item, classes, direction) {
        const resizer = document.createElement('span');
        resizer.classList.add(...classes);
        if (direction) {
            resizer.dataset.direction = direction;
        }        
        item.appendChild(resizer);
    }

    moveMeDrag(elem, e) {
        const { top, bottom, left, right } = elem.getBoundingClientRect();
        const gridColumnStart = parseInt(elem.dataset.gridColumnStart, 10);
        const gridColumnEnd = parseInt(elem.dataset.gridColumnEnd, 10);
        const gridRowStart = parseInt(elem.dataset.gridRowStart, 10);
        const gridRowEnd = parseInt(elem.dataset.gridRowEnd, 10);
        if (top - this.moveMeActions.gap > e.clientY && gridRowStart > 1) {
            elem.dataset.gridRowStart = gridRowStart - 1;
            elem.dataset.gridRowEnd = gridRowEnd - 1;
        } else if (bottom + this.moveMeActions.gap < e.clientY && gridRowEnd < 13) {
            elem.dataset.gridRowStart = gridRowStart + 1;
            elem.dataset.gridRowEnd = gridRowEnd + 1;
        } else if (left - this.moveMeActions.gap > e.clientX && gridColumnStart > 1) {
            elem.dataset.gridColumnStart = gridColumnStart - 1;
            elem.dataset.gridColumnEnd = gridColumnEnd - 1;
        } else if (right + this.moveMeActions.gap < e.clientX && gridColumnEnd < 13) {
            elem.dataset.gridColumnStart = gridColumnStart + 1;
            elem.dataset.gridColumnEnd = gridColumnEnd + 1;
        }
    }

    moveMeStop(elem, e) {
        console.log('moveme stop');
        window.removeEventListener('mousemove', this.moveMeActions.drag);
        window.removeEventListener('mouseup', this.moveMeActions.stop);
        delete this.moveMeActions;
        this.hideTemplateGrid();
        this.checkClash(elem);
    }

    moveMeInit(e) {
        const elem = e.target.parentElement;
        const gap = parseInt(elem.closest('[data-grid-gap]').dataset.gridGap) || 0;
        this.moveMeActions = {
            drag: this.moveMeDrag.bind(this, elem),
            stop: this.moveMeStop.bind(this, elem),
            initialPoint: { x: e.clientX, y: e.clientY },
            gap,
        }
        window.addEventListener('mousemove', this.moveMeActions.drag);
        window.addEventListener('mouseup', this.moveMeActions.stop);
        this.showTemplateGrid();
    }

    bindActionIcons() {
        document.querySelector('.grid > *:not([data-binded="true"]) .icon.remove').addEventListener('click', (e)=>{
            if (confirm('Are you sure you want to remove this item??')) {
                e.target.parentElement.remove();
            }
        });
        document.querySelector('.grid > *:not([data-binded="true"]) .icon.edit').addEventListener('click', (e)=>{
            new EditPopup(e.target.parentElement, this.bindResizables.bind(this));
        });        
        document.querySelector('.grid > *:not([data-binded="true"]) .icon.moveMe').addEventListener('mousedown', this.moveMeInit.bind(this));
    }

    resizableLoad(item) {
        this.createSpanElement(item, ['resizer'], 'right');
        this.createSpanElement(item, ['resizer'], 'left');
        this.createSpanElement(item, ['resizer'], 'up');
        this.createSpanElement(item, ['resizer'], 'down');
        this.createSpanElement(item, ['icon', 'remove']);
        this.createSpanElement(item, ['icon', 'moveMe']);
        this.createSpanElement(item, ['icon', 'edit']);
        this.bindActionIcons();
        item.setAttribute('data-binded', 'true');        
    }

    resizableResize(elem, e) {
        const gridColumnStart = parseInt(elem.dataset.gridColumnStart, 10);
        const gridColumnEnd = parseInt(elem.dataset.gridColumnEnd, 10);
        const gridColumnDiff = gridColumnEnd - gridColumnStart;
        const gridRowStart = parseInt(elem.dataset.gridRowStart, 10);
        const gridRowEnd = parseInt(elem.dataset.gridRowEnd, 10);
        const gridRowDiff = gridRowEnd - gridRowStart;
        const halfColumn = this.gridColumnWidth / 2 + this.gridGapWidth;
        switch (this.direction) {
            case 'right': {
                const elemPosX = elem.getBoundingClientRect().right;
                const draggedDistance = elemPosX - e.clientX;
                if (draggedDistance < 0 && gridColumnEnd < 13 && e.clientX > elemPosX + halfColumn) {
                    elem.dataset.gridColumnEnd = gridColumnEnd + 1;
                } else if (draggedDistance > 0 && gridColumnDiff > 1 && e.clientX < elemPosX - halfColumn) {
                    elem.dataset.gridColumnEnd = gridColumnEnd - 1;
                }
            }
                break;
            case 'left': {
                const elemPosX = elem.getBoundingClientRect().left;
                const draggedDistance = elemPosX - e.clientX;
                if (draggedDistance > 0 && gridColumnStart > 1 && e.clientX < elemPosX - halfColumn) {
                    elem.dataset.gridColumnStart = gridColumnStart - 1;
                } else if (draggedDistance < 0 && gridColumnDiff > 1 && e.clientX > elemPosX + halfColumn) {
                    elem.dataset.gridColumnStart = gridColumnStart + 1;
                }
            }
                break;
            case 'up': {
                const elemPosY = elem.getBoundingClientRect().top;
                if (!this.lastChange) {
                    this.lastChange = e.clientY;
                }
                const draggedDistance = this.lastChange - e.clientY;
                if (draggedDistance < -20 && elemPosY < e.clientY - 20 && gridRowDiff > 1 && gridRowStart > 0) { // dragging down
                    elem.dataset.gridRowStart = gridRowStart + 1;
                    this.lastChange = e.clientY;
                } else if (draggedDistance > 20 && elemPosY > e.clientY + 20 && gridRowDiff > 0 && gridRowStart > 1) { // dragging up
                    elem.dataset.gridRowStart = gridRowStart - 1;
                    this.lastChange = e.clientY;
                }
            }
                break;
            case 'down': {
                const elemPosY = elem.getBoundingClientRect().bottom;
                if (!this.lastChange) {
                    this.lastChange = e.clientY;
                }
                const draggedDistance = this.lastChange - e.clientY;
                if (draggedDistance < -20 && elemPosY < e.clientY - 20 && gridRowEnd < 13 && gridRowDiff > 0) { // dragging down
                    elem.dataset.gridRowEnd = gridRowEnd + 1;
                    this.lastChange = e.clientY;
                } else if (draggedDistance > 20 && elemPosY > e.clientY + 20 && gridRowEnd < 14 && gridRowDiff > 1) { // dragging up
                    elem.dataset.gridRowEnd = gridRowEnd - 1;
                    this.lastChange = e.clientY;
                }
            }
                break;
        }
    }

    resizableInit(e) {
        const { column, gap } = this.gridColumnDimensions(e.target);
        this.gridColumnWidth = column;
        this.gridGapWidth = gap;
        this.direction = e.target.dataset.direction;

        if (this.direction === 'up' || this.direction === 'down') {            
            document.body.style.cursor = 'n-resize';
        } else {
            document.body.style.cursor = 'e-resize';
        }

        this.showTemplateGrid();
    }

    resizableStop(e) {
        delete this.direction;
        delete this.gridColumnWidth;
        delete this.gridGapWidth;
        delete this.lastChange;
        document.body.style.cursor = '';        

        this.hideTemplateGrid();
    }

    gridColumnDimensions(clickedElem) {
        const containerWidth = document.querySelector('.grid').getBoundingClientRect().width;
        const gap = clickedElem.closest('[data-grid-gap]').dataset.gridGap || 0;
        const spaceWastedInGaps = parseInt(gap, 10) * 11;
        const column = (containerWidth - spaceWastedInGaps) / 12;
        return { column, gap: parseInt(gap, 10) };
    }

    dragStart(e) {
        this.showTemplateGrid();
        e.dataTransfer.setData('text/plain', e.target.dataset.wcelement);
    }

    dragEnd(e) {
        this.hideTemplateGrid();
    }

    dragEnter(e) {
        e.target.style.backgroundColor = 'red';
        e.preventDefault();
    }

    dragLeave(e) {
        e.target.style.backgroundColor = '';
    }

    drop(e) {
        const index = Array.from(e.target.parentElement.children).indexOf(e.target);
        const elemType = e.dataTransfer.getData('text/plain');
        const theGrid = document.querySelector('.grid');
        const igwsElement = document.createElement(elemType);
        igwsElement.dataset.gridColumnStart = index + 1;
        igwsElement.dataset.gridColumnEnd = index + 2;
        // Check the lowest row available
        const lowestRowStart = this.detectLowestRow(theGrid, index + 1);
        igwsElement.dataset.gridRowStart = lowestRowStart;
        igwsElement.dataset.gridRowEnd = lowestRowStart + 1;
        igwsElement.textContent = elemType;
        theGrid.appendChild(igwsElement);
        e.target.style.backgroundColor = '';
        this.bindResizables();
    }

    detectLowestRow(grid, columnStart) {
        return Array.from(grid.children).reduce((acc = 1, item) => {
            const gridColumnStart = parseInt(item.dataset.gridColumnStart, 10);
            const gridColumnEnd = parseInt(item.dataset.gridColumnEnd, 10);
            const gridRowEnd = parseInt(item.dataset.gridRowEnd, 10);
            if ((gridColumnStart === columnStart ||
                (gridColumnStart < columnStart && gridColumnEnd > columnStart)) &&
                gridRowEnd > acc) {
                return gridRowEnd;
            }
            return acc;
        }, 1);
    }

    showTemplateGrid() {
        document.querySelector('.templater').classList.add('templater--highlighted');
        document.querySelector('.grid').classList.add('grid--highlight');
    }

    hideTemplateGrid() {
        document.querySelector('.templater').classList.remove('templater--highlighted');
        document.querySelector('.grid').classList.remove('grid--highlight');
    }

    checkClash(elem) {
        document.querySelectorAll('.grid--clash').forEach(elem => elem.classList.remove('grid--clash'));
        const grid = elem.closest('.grid');
        const elemColumnStart = parseInt(elem.dataset.gridColumnStart, 10);
        const elemColumnEnd = parseInt(elem.dataset.gridColumnEnd, 10);
        const elemRowStart = parseInt(elem.dataset.gridRowStart, 10);
        const elemRowEnd = parseInt(elem.dataset.gridRowEnd, 10);
        const clash = Array.from(grid.children).filter(child => {
            const childColumnStart = parseInt(child.dataset.gridColumnStart, 10);
            const childColumnEnd = parseInt(child.dataset.gridColumnEnd, 10);
            const childRowStart = parseInt(child.dataset.gridRowStart, 10);
            const childRowEnd = parseInt(child.dataset.gridRowEnd, 10);
            if (child !== elem && 
                (((elemColumnStart >= childColumnStart && elemColumnStart < childColumnEnd) ||
                elemColumnEnd <= childColumnEnd && elemColumnEnd > childColumnStart) && (
                (elemRowStart >= childRowStart && elemRowStart < childRowEnd) || 
                (elemRowEnd <= childRowEnd && elemRowEnd > childRowStart)
                ))) {
                    return true;
            }
            return false
        });
        if (clash.length > 0) {
            clash.forEach(elem => elem.classList.add('grid--clash'));
            elem.classList.add('grid--clash');
        }
    }
}