export default class SideKick {
    constructor(elements = {}) {
        this.container = document.querySelector("body");
        this.sideKick = document.querySelector('#sideKick');
        this.sideKickDragger = document.querySelector('#sideKick__title');
        const elementList = this.sideKick.querySelector('#sideKick__elements');
        Object.keys(elements).forEach(elem => {
            elementList.insertAdjacentHTML('beforeend', `<li class="drag--draggable" draggable="true" data-wcelement="${elem}">${elem}</li>`)
        });

        if (this.sideKick) {
            this.sideKickDragger.addEventListener('mousedown', this.startMoving.bind(this));
            this.sideKickDragger.addEventListener('mouseup', this.stopMoving.bind(this));
        }
    }

    reposition(posX, posY) {
        this.sideKick.style.left = `${posX}px`;
        this.sideKick.style.top = `${posY}px`;
    }

    startMoving(evt = window.event) {
        const posX = parseInt(evt.clientX, 10);
        const posY = parseInt(evt.clientY, 10);
        const {
            top,
            left,
            width,
            height
        } = this.sideKick.getBoundingClientRect();
        const containerWidth = parseInt(window.innerWidth, 10);
        const containerHeight = parseInt(window.innerHeight, 10);
        this.sideKick.style.cursor = 'move';
        const diffX = posX - left;
        const diffY = posY - top;
        // This is to avoid multiple bindings like when the mouse goes out of the screen and back in
        if (this.bindedMouseMove) {
            document.removeEventListener('mousemove', this.bindedMouseMove);
        }
        this.bindedMouseMove = this.mouseMove.bind(this, diffX, diffY, width, height, containerWidth, containerHeight);
        document.addEventListener('mousemove', this.bindedMouseMove);
    }

    stopMoving() {
        this.sideKick.style.cursor = 'default';
        document.removeEventListener('mousemove', this.bindedMouseMove);
    }

    mouseMove(diffX, diffY, elementWidth, elementHeight, containerWidth, containerHeight, evt = window.event) {
        const posX = evt.clientX;
        const posY = evt.clientY;
        let newX = posX - diffX;
        let newY = posY - diffY;
        if (newX < 0) newX = 0;
        if (newY < 0) newY = 0;
        if (newX + elementWidth > containerWidth) newX = containerWidth - elementWidth;
        if (newY + elementHeight > containerHeight) newY = containerHeight - elementHeight;
        this.reposition(newX, newY);
    }
}