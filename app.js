import SideKick from './scripts/SideKick';
import Layout from './scripts/Layout';
import components from './components.json';

(() => {   
    new SideKick(components);    
    new Layout();
})();